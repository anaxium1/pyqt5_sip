# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location

mkdir qt
cd qt
$qtpath = Get-Location
curl.exe -L -o qt.zip https://gitlab.com/anaxium1/qt5/-/jobs/artifacts/master/raw/qt.zip?job=job
7z x qt.zip
rm qt.zip

cd $toppath
mkdir python
cd python
$pythonpath = Get-Location
Copy-Item -Path "C:\Python36-32\*" -Destination $pythonpath -Recurse
curl.exe -L -o sip.zip https://gitlab.com/anaxium1/sip/-/jobs/artifacts/master/raw/sip.zip?job=job
curl.exe -L -o pyqt5.zip https://gitlab.com/anaxium1/pyqt5/-/jobs/artifacts/master/raw/pyqt5.zip?job=job
7z x sip.zip
7z x pyqt5.zip
$env:Path = "$pythonpath;$env:Path"

cd $toppath
curl.exe -L -o PyQt5_sip-12.9.0.tar.gz https://files.pythonhosted.org/packages/b1/40/dd8f081f04a12912b65417979bf2097def0af0f20c89083ada3670562ac5/PyQt5_sip-12.9.0.tar.gz
7z x -y PyQt5_sip-12.9.0.tar.gz
7z x -y PyQt5_sip-12.9.0.tar
cd PyQt5_sip-12.9.0
python setup.py install

cd $toppath
mkdir out
cd out
$outpath = Get-Location
mkdir Lib
cd Lib
mkdir site-packages
cd $outpath
Copy-Item -Path "$pythonpath\Lib\site-packages\PyQt5_sip-12.9.0-py3.6-win32.egg\PyQt5" -Destination "$outpath\Lib\site-packages" -Recurse
7z a -r ..\pyqt5_sip.zip *
